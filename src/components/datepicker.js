import React, { Component, useState } from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DateTimePicker = ({ type, buttonTitle, dateKey, setSchedule }) => {
  // Estado para controlar a visibilidade do seletor de data/hora
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  // Função para mostrar o seletor de data/hora
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  // Função para ocultar o seletor de data/hora
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  // Função para lidar com a confirmação da seleção de data/hora
  const handleConfirm = (date) => {
    // Verifica se é seleção de hora
    if (type === "time") {
      // Extrai apenas hora e minuto
      const hour = date.getHours();
      const minute = date.getMinutes();

      // Formata hora e minuto como desejado
      const formattedTime = `${hour}:${minute}`;

      // Atualiza o estado com a hora e o minuto formatados
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedTime,
      }));
    } else {
      // Se não for seleção de hora, atualiza o estado com a data selecionada
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: date,
      }));
    }
    // Oculta o seletor de data/hora após a confirmação
    hideDatePicker();
  };

  return (
    <View>
      {/* Botão para abrir o seletor de data/hora */}
      <Button title={buttonTitle} onPress={showDatePicker} color="black" />
      {/* Componente de seletor de data/hora */}
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode={type}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
        // Ajusta o estilo do modal
        pickerContainerStyleIOS={{ backgroundColor: "#fff" }}
        textColor="#000" // Cor do texto
      />
    </View>
  );
};
export default DateTimePicker;
